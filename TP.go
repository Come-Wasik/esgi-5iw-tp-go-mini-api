package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"
)

// Handlers
func welcomeHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		now := time.Now()
		fmt.Fprintf(w, "%dh%d\n", now.Hour(), now.Minute())
	default:
		Send404NotFound(w)
	}
}

func AddEntryHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			Send500InternalServerError(w)
			return
		}

		// We check the form keys
		fmt.Println(req.Form.Has("entry"))
		if !req.Form.Has("entry") || !req.Form.Has("author") {
			send500InvalidForm(w)
			return
		}

		// Open the data file
		data := string(req.Form.Get("author") + ":" + req.Form.Get("entry"))
		writeIntoFile("data.txt", data, w)

		// Send the result to the user
		fmt.Fprintf(w, "%s\n", data)
	default:
		Send404NotFound(w)
	}
}

func EntriesHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		entries := getEntriesFromFile("data.txt", w)
		for _, entry := range entries {
			fmt.Fprintln(w, entry)
		}

	default:
		Send404NotFound(w)
	}
}

// Errors
func Send404NotFound(w http.ResponseWriter) {
	fmt.Fprintln(w, "Page not found")
	fmt.Println("Page not found")
}

func Send500InternalServerError(w http.ResponseWriter) {
	fmt.Fprintln(w, "Something went wrong")
	fmt.Println("Something went wrong")
}

func send500InvalidForm(w http.ResponseWriter) {
	fmt.Fprintln(w, "Invalid form")
	fmt.Println("Invalid form")
}

// Functional
func writeIntoFile(filename string, data string, w http.ResponseWriter) {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		Send500InternalServerError(w)
		fmt.Println(err)
		return
	}
	// Do and control the file writing
	_, err = fmt.Fprintln(file, data)
	if err != nil {
		Send500InternalServerError(w)
		fmt.Println(err)
		file.Close()
		return
	}
	// Close and control the file closing
	err = file.Close()
	if err != nil {
		Send500InternalServerError(w)
		fmt.Println(err)
		return
	}
}

func getEntriesFromFile(filename string, w http.ResponseWriter) []string {
	error := []string{}
	// Open the file
	file, err := os.Open(filename)
	if err != nil {
		Send500InternalServerError(w)
		fmt.Println(err)
		return error
	}

	// Read line per line
	scanner := bufio.NewScanner(file)
	var lines []string
	for scanner.Scan() {
		// On ne récupère que l'entrée
		explodedData := strings.Split(scanner.Text(), ":")
		lines = append(lines, explodedData[1])
	}
	if err := scanner.Err(); err != nil {
		Send500InternalServerError(w)
		fmt.Println(err)
	}

	// Close the file
	err = file.Close()
	if err != nil {
		Send500InternalServerError(w)
		fmt.Println(err)
		return error
	}

	return lines
}

func main() {
	PORT := "4567"
	http.HandleFunc("/", welcomeHandler)
	http.HandleFunc("/add", AddEntryHandler)
	http.HandleFunc("/entries", EntriesHandler)
	fmt.Println("Listening on http://localhost:" + PORT)
	http.ListenAndServe(":"+PORT, nil)
}
